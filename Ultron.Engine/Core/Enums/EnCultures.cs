﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ultron.Engine.Core.Enums
{
    public enum EnCultures
    {
        GER = 1,
        ENG = 2
    }
}
